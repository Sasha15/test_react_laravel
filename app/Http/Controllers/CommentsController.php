<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentsController extends Controller
{
    public function index()
    {
        return Comment::orderBy("id", 'DESC')->get();
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:25',
            'comment' => 'required|max:25'
        ]);

        $comment = Comment::create($request->all());

        return response()->json($comment, 201);
    }
}