class CommentApi {
    static getAllComments() {
        return fetch('/comments').then(response => {
            return response.json();
        }).catch(error => {
            return error;
        });
    }

    static createComment(comment) {

        const headers = Object.assign({'Accept': 'application/json', 'Content-Type': 'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')});
        const request = new Request(`/comments`, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify({name: comment.name, comment: comment.comment})
        });


        return fetch(request).then(response => {
            console.log(response)
            return response.json();
        }).catch(error => {
            return error;
        });
    }
}

export default CommentApi;