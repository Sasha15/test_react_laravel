import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/Main';
import Comments from './components/comments/Comments';

export default (
    <Route path="/" component={App}>
        <IndexRoute component={Comments} />
    </Route>
);