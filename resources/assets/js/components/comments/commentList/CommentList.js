import React, {PropTypes} from 'react';
import {Link} from 'react-router';


const CommentList = ({comments}) => {
    return (
        <div >
            {comments.map(comment =>
                <div key={comment.id} className="mr-5">
                    <span><img src="https://cdn1.iconfinder.com/data/icons/unique-round-blue/93/user-512.png" className="col-md-1 rounded-circle"/></span>
                    <p className="col-md-11 mb-3">
                        <span className="text-muted">{comment.name}</span>
                        <br/>
                        {comment.comment}
                    </p>
                </div>
            )}
        </div>
    );
};

CommentList.propTypes = {
    comments: PropTypes.array.isRequired
};

export default CommentList;