import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import CommentForm from "./commentForm/CommentForm";
import CommentList from "./commentList/CommentList";
import * as commentActions from '../../actions/commentActions';

class Comments extends React.Component {
    constructor(props, context, state) {
        super(props, context, state);
        this.state={
            comments: this.props.comments,
            comment: []
        };
        this.saveComment = this.saveComment.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.comments !== nextProps.comments) {
            this.setState({comments: nextProps.comments});
        }
    }

    render() {
        const {comments} = this.state;

        return (
            <div className="col-md-6">
                <div>
                    <img src="https://cdn1.iconfinder.com/data/icons/unique-round-blue/93/user-512.png"
                           className="col-md-1 rounded-circle"
                    />
                    <div className="col-md-11">
                        <CommentForm
                            onSave={this.saveComment}
                        />
                    </div>
                </div>
                <hr/>
                <div className="col-md-12">
                    {comments.length ? <CommentList comments={comments} /> : 'No comments'}
                </div>
            </div>
        );
    }

    saveComment(comment) {
        this.props.actions.createComment(comment)
    }
}


Comments.propTypes = {
    comments: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        comments: state.comments
    };
}


function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(commentActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Comments);