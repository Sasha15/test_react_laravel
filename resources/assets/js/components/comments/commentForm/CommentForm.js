import React from 'react';
import TextInput from '../../common/TextInput';

class CommentForm extends React.Component {
    constructor(props, state) {
        super(props, state);
        this.state={
            comment: [],
            error: true
        };
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
    }

    render() {
        return (
            <form>
                <div className="col-md-4">
                    <TextInput
                        name="name"
                        placeholder={"Name"}
                        onChange={this.onChange}
                    />
                </div>
                <div className="col-md-11">
                    <TextInput
                        name="comment"
                        placeholder={"Add a comment..."}
                        onChange={this.onChange}
                        error={this.state.error ? '' : 'Must be less than 25 symbols'}
                    />
                </div>
                <div className="col-md-1">
                    <input
                        type="button"
                        className="btn btn-primary"
                        value={"Create"}
                        onClick={this.onSave}/>
                    {" "}
                </div>
            </form>
        );
    }

    onChange (event) {

        const comment = this.state.comment;

        const field=event.target.name;

        comment[field]=event.target.value;
    }

    onSave () {

        const {comment} = this.state;
        if(this.checkValidation(comment.comment)){
            this.props.onSave(comment)

        }

    }

    checkValidation(value) {
        const field = value.length <= 25;
        this.setState({error: field});
        return field;
    }
}


CommentForm.propTypes = {
    onSave: React.PropTypes.func.isRequired,
    saving: React.PropTypes.bool
};


export default CommentForm;