import commentApi from '../api/commentApi';

export function loadComments() {
    return function(dispatch) {
        return commentApi.getAllComments().then(comments => {
            dispatch(loadCommentsSuccess(comments));
        }).catch(error => {
            throw(error);
        });
    };
}

export function createComment(comment) {
    return function (dispatch) {
        return commentApi.createComment(comment).then(responseComment => {
            dispatch(createCommentSuccess(responseComment));

            return responseComment;
        }).catch(error => {
            throw(error);
        });
    };
}


export function loadCommentsSuccess(comments) {
    return {type: 'LOAD_COMMENTS_SUCCESS', comments};
}

export function createCommentSuccess(comment) {
    return {type: 'CREATE_COMMENT_SUCCESS', comment};
}