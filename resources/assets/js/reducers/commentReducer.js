import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function commentReducer(state = initialState.comments, action) {
    switch(action.type) {
        case types.LOAD_COMMENTS_SUCCESS:
            return action.comments;
        case types.CREATE_COMMENT_SUCCESS:
            return [
                Object.assign({}, action.comment),
                ...state.filter(comment => comment.id !== action.comment.id)
            ];

        default:
            return state;
    }
}